using Plots

# Define your three float values
y = [
  2.327473685,
  38.339006423950195,
  0.258953
]
x = [1, 2, 3]
# Define labels for the categories (optional)
categories = ["Rust", "Python", "Julia"]

# Create the chart
p = plot(
  bar(
    x, y,
    bar_width=1,
    color=[:blue, :green, :red],
    xticks=(x, categories),
    label="Bar",
    xlabel="Programming language",
    ylabel="Runtime in Seconds"
  ),
  fmt=:pdf
)
savefig(p, "runtimePlots.pdf")
