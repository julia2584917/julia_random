abstract type Tier; 
end
struct Vogel <: Tier; 
    name::String
    kind::String
    flying::Bool
end
Vogel() = new("Wellensittich", "bird", true)
struct Cat <: Tier
    name::String
    kind::String
    flying::Bool
    Cat(name, kind, flying) = flying ? new(name, "No_Cat_type", flying) : new(name, kind, flying)
end
struct Mouse <: Tier;
    name::String
    kind::String
    flying::Bool
end