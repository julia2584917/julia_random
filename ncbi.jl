using BenchmarkTools
function align_sequences()
  fasta_file = open("./shrinked_sclerot.fasta")
  sequences = String[]
  for line in eachline(fasta_file)
    if startswith(line, '>')
      push!(sequences, "")
    else
      sequences[end] *= line
    end
  end
  for seqS in sequences
    for seqT in sequences
      if seqS == seqT
        local_pairwise(seqS,seqT)
      end
    end
  end
end
function local_pairwise(seq1,seq2)
  S = zeros(Int, length(seq1)+1, length(seq2)+1)
  for i in 1:length(seq1)
        for j in 1:length(seq2)
            S[i+1,j+1] = max(
              S[i,j] + (seq1[i] == seq2[j] ? 1 : -1),
              S[i,j+1] - 1,
              S[i+1,j] - 1)
        end
    end
  return S[length(seq1),length(seq2)]
end
@time align_sequences()
