using Plots

# Define your three float values
y = [5.2 3.8 7.1]
x = [1, 2, 3]
# Define labels for the categories (optional)
categories = ["Category 1", "Category 2", "Category 3"]

# Create the chart
p = plot(x, y
)
savefig(p, "myplot.pdf")
